using RestSharp;

public class DocspellClient : IDocspell
{
    private readonly IRestClient _restClient;

    public DocspellClient(IRestClient restClient){
        _restClient = restClient;
    }

    public Task<string> Authenticate(string username, string password)
    {
        var request = new RestRequest("/open/auth/login", Method.Get);
        _restClient.GetAsync(request);

        throw new NotImplementedException();
    }

    public Task<string> DownloadItems()
    {
        throw new NotImplementedException();
    }
}