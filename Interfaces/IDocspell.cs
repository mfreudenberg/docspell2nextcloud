public interface IDocspell {
    public Task<string> Authenticate(string username, string password);
    public Task<string> DownloadItems();
}